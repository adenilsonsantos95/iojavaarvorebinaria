package JogoAdivinha.Test;



import JogoAdivinha.BinaryTree.BinaryTree;
import JogoAdivinha.BinaryTree.NodeBinaryTree;
import JogoAdivinha.Questions.Questions;
import com.google.gson.reflect.TypeToken;
import iojava.IOJson;

import javax.swing.*;
import java.io.File;
import java.lang.reflect.Type;
import java.util.Scanner;

import static java.lang.Thread.*;

public class Main {
    static BinaryTree<Questions> arvore = new BinaryTree<Questions>();
    static Scanner input = new Scanner(System.in);


    public static void main(String[] args) throws Exception {
        IOJson caminho = arquivoJogo();
        if (resgatarJogo(caminho) != null){
            arvore = resgatarJogo(caminho);
        }
        int opcao = 0;
        do {
            System.out.println("------- JOGO DE ADVINHA -------\n" +
                    "1 - Jogar\n" +
                    "2 - Inserir Pergunta\n" +
                    "0 - Sair");
            opcao = input.nextInt();
            switch (opcao){
                case 1:
                    if (arvore.isEmpty()){
                        alimentarJogo();
                        break;
                    }else{
                       jogar();
                       break;
                    }
                case 2:
                    alimentarJogo();
                    break;
            }
        }while (opcao != 0);
        salvarJogo(arvore, caminho);


    }

    public static void jogar(){
        perguntarJogo(arvore.getRoot());
        return;
    }

    private static void perguntarJogo(NodeBinaryTree<Questions> root) {
        System.out.println(root.getElement().getQuestion() + "  1 - SIM   0 - NAO");
        int resposta = input.nextInt();
        if (resposta == 1){
            System.out.println("A resposta é: " + root.getElement().getAnswer().toUpperCase() + " se eu Acertei aperte (1) senao (0)");
            int resp = input.nextInt();
            if (resp == 1){
                System.out.println("Acertei!!!");
                return;
            }else{
                if (root.getRigth() != null){
                    perguntarJogo(root.getRigth());
                }
            }
        }else{
            System.out.println("Que pena que errei, pensei que a resposta era: " + root.getElement().getAnswer().toUpperCase());
            if (root.getLeft() != null){
                perguntarJogo(root.getLeft());
            }
        }
        if (root.getLeft() == null || root.getRigth() == null) {
            System.out.println("Nao tenho mais opções. Obrigado por Jogar!!!");
        }

    }

    //
    //METODO QUE ALIMENTA O JOGO
    //
    public static void alimentarJogo(){
        String pergunta = JOptionPane.showInputDialog(null, "Informe a Pergunta: ");
        String resposta = JOptionPane.showInputDialog(null, "Informe a Resposta: ");
        Questions questao = new Questions(pergunta, resposta);
        arvore.inserirPergunta(questao);
    }

    public static IOJson arquivoJogo()throws Exception{
        String path = "teste.json";
        File file = new File(path);
        if (file.exists()){
            return new IOJson(path);
        }else{
            BinaryTree<Questions> arvore = new BinaryTree<>();
            IOJson iOJson = new IOJson(path);
            iOJson.write(arvore);
            return iOJson;
        }
    }

    public static void salvarJogo(BinaryTree<Questions> arvore, IOJson p){
        try {
            p.write(arvore);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static BinaryTree<Questions> resgatarJogo(IOJson p) throws Exception {
        Type type = new TypeToken<BinaryTree<Questions>>(){}.getType();
        BinaryTree<Questions> arv = (BinaryTree<Questions>)p.read(type);
        return arv;
    }
}
