package JogoAdivinha.BinaryTree;

public class NodeBinaryTree<T extends Comparable<T>> {
    private T element;
    private NodeBinaryTree<T> left;
    private NodeBinaryTree<T> rigth;

    public NodeBinaryTree(T element) {
        this.element = element;
        this.left = this.rigth = null;
    }

    public T getElement() {
        return element;
    }

    public void setElement(T element) {
        this.element = element;
    }

    public NodeBinaryTree<T> getLeft() {
        return left;
    }

    public void setLeft(NodeBinaryTree<T> left) {
        this.left = left;
    }

    public NodeBinaryTree<T> getRigth() {
        return rigth;
    }

    public void setRigth(NodeBinaryTree<T> rigth) {
        this.rigth = rigth;
    }
}
