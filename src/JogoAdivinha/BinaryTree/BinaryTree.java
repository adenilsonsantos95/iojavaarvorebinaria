package JogoAdivinha.BinaryTree;

import java.io.Serializable;

public class BinaryTree<T extends Comparable<T>>  implements IBinaryTree<T>, Serializable {
    private NodeBinaryTree<T> root;
    int size;

    public BinaryTree(){
        this.root = null;
        this.size = 0;
    }

    public NodeBinaryTree<T> getRoot() {
        return root;
    }

    @Override
    public void add(T element) throws ExceptionTree {
        if ( element == null) throw new ExceptionTree("O elemento é nulo!");
        else if (this.root == null){
            this.root = new NodeBinaryTree<T>(element);

        }else{
            NodeBinaryTree<T> no = new NodeBinaryTree<T>(element);
            this.addInternal(this.root, no);
        }
        this.size++;
    }

    private void addInternal(NodeBinaryTree<T> internal, NodeBinaryTree<T> external){
        if (internal.getElement().compareTo(external.getElement()) > 0){
            if (internal.getLeft() == null) internal.setLeft(external);
            else this.addInternal(internal.getLeft(), external);

        }else if(internal.getElement().compareTo(external.getElement()) < 0){
            if (internal.getRigth() == null) internal.setRigth(external);
            else this.addInternal(internal.getRigth(), external);
        }
    }

    @Override
    public boolean contains(T element) throws ExceptionTree {
        if (element == null) throw new ExceptionTree("O elemento é nulo!");
        if (this.root == null) throw new ExceptionTree("A arvore está vazia!");
        if (this.root.getElement().compareTo(element) == 0) return true;
        return this.containsInternal(this.root, element);

    }

    private boolean containsInternal(NodeBinaryTree<T> internal, T element){
        if (internal == null) return false;
        if (element.compareTo(internal.getElement()) < 0)
            return this.containsInternal(internal.getLeft(), element);
        if (element.compareTo(internal.getElement()) > 0)
            return this.containsInternal(internal.getRigth(), element);
        else
        return true;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return this.root == null;
    }

    private void inserirPerguntaRigth(NodeBinaryTree<T> no, T elemento){
        if (no.getRigth() != null){
            this.inserirPerguntaInterno(no.getRigth(), elemento);
        }else{
            NodeBinaryTree<T> novoNo = new NodeBinaryTree<T>(elemento);
            no.setRigth(novoNo);
        }
    }

    private void inserirPerguntaLeft(NodeBinaryTree<T> no, T elemento){
        if (no.getLeft() != null){
            this.inserirPerguntaLeft(no.getLeft(), elemento);
        }else{
            NodeBinaryTree<T> novoNo = new NodeBinaryTree<T>(elemento);
            no.setLeft(novoNo);
        }
    }

    private void inserirPerguntaInterno(NodeBinaryTree<T> no, T elemento){
        if (this.root == null){
            NodeBinaryTree<T> novoNo = new NodeBinaryTree<T>(elemento);
            this.root = novoNo;
        }else if (no.getLeft() == null){
            inserirPerguntaLeft(no, elemento);
        }else{
            inserirPerguntaRigth(no, elemento);
        }
    }

    public void inserirPergunta(T elemento){
        this.inserirPerguntaInterno(this.root, elemento);
        this.size++;
    }


    @Override
    public void remove(T element) throws ExceptionTree {
        if (!this.contains(element) || element == null) throw new ExceptionTree("O elemento não foi encontrado ou é nulo!");
        if (root == null) throw new ExceptionTree("Arvore vazia!");
        else this.removeInternal(root, element);
    }

    private void removeInternal(NodeBinaryTree<T> internal, T element){
        if (element.compareTo(internal.getElement()) < 0)
            this.removeInternal(internal.getLeft(), element);
        else if (element.compareTo(internal.getElement()) > 0)
            this.removeInternal(internal.getRigth(), element);
        else {
            NodeBinaryTree<T> aux = internal;
            if ( aux.getRigth() == null) internal = aux.getLeft();
            else if (aux.getLeft() == null) internal = aux.getRigth();
            else this.removeInternalMinimum(aux, aux.getRigth());
            aux = null;
        }
    }

    private void removeInternalMinimum(NodeBinaryTree<T> x, NodeBinaryTree<T> y){
        if (y.getLeft() != null)
            this.removeInternalMinimum(x, y.getLeft());
        else {
            x.setElement(y.getElement());
            x = y;
            y = y.getRigth();
        }
    }

    private void listInternal(NodeBinaryTree<T> node){
        if (node != null){
            listInternal(node.getLeft());
            System.out.print(node.getElement() + " ");
            listInternal(node.getRigth());
        }
    }


    public void list() {
        listInternal(root);
    }
}
