package JogoAdivinha.BinaryTree;

public interface IBinaryTree<T extends Comparable<T>> {
    void add(T elemento) throws ExceptionTree;
    boolean contains(T elemento) throws ExceptionTree;
    int size();
    boolean isEmpty();
    void remove(T elemento) throws ExceptionTree;
}
